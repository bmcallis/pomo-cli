const axios = require('axios');
const querystring = require('querystring');

const get = (path, token) => {
  return axios.post(`https://slack.com/api/${path}?token=${token}`)
    .then(response => {
      return response.data.ok ? Promise.resolve(response.data) : Promise.reject(response.data.error);
    });
};

const post = (path, token, params) => {
  const url = `https://slack.com/api/${path}?token=${token}`;
  const body = querystring.stringify(params);
  const config = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  };

  return axios.post(url, body, config)
    .then(response => {
      if (response.data.ok) {
        return Promise.resolve(response.data);
      }

      console.log(response.data);
      return Promise.reject(response.data.error);
    });
};

module.exports = {
  startDnd: (token, minutes) => post('dnd.setSnooze', token, { num_minutes: minutes }),
  endDnd: token => get('dnd.endDnd', token),
  getStatus: token => get('users.profile.get', token),
  setStatus: (token, text = '', emoji = '') => {
    return post('users.profile.set', token, {
      profile: JSON.stringify({
        status_text: text,
        status_emoji: emoji ? `:${emoji}:` : '',
      }),
    });
  },
};
