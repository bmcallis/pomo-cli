module.exports = {
  extends: [
    'bmcallis/core',
    'bmcallis/babel',
  ],
  rules: {
    'camelcase': 'off',
    'multiline-comment-style': 'off',
    'no-console': 'off',
    'no-process-exit': 'off',
    'semi': ['error', 'always'],
  }
}

