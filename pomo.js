#!/usr/bin/env node

const program = require('commander');
const notifier = require('node-notifier');
const { Signale } = require('signale');
const addMinutes = require('date-fns/add_minutes');
const formatDistanceStrict = require('date-fns/distance_in_words_strict');
const on_death = require('death')({ uncaughtException: true });
const {
  getStatus,
  setStatus: setSlackStatus,
  startDnd,
  endDnd,
} = require('./slack');
const { version } = require('./package.json');

const signale = new Signale();
const progress = new Signale({ interactive: true });

const origStatus = {};
let mins, intervalId;

program
  .version(version)
  .arguments('[mins]')
  .action((val = 25) => {
    mins = parseInt(val, 10);
  })
  .option('-t, --text <text>', 'Status text', 'Pomodoro')
  .option('-e, --emoji <emoji>', 'Emoji name', 'tomato')
  .option('--no-slack', 'Disable slack status updates')
  .option('--no-notify', 'Disable OS notification')
  .option('--debug', 'Print debug statements')
  .parse(process.argv);

const token = process.env.SLACK_CLI_TOKEN;
if (program.slack && !token) {
  signale.error(`Environment variable 'SLACK_CLI_TOKEN' must be set to slack api token.
  Go to https://api.slack.com/custom-integrations/legacy-tokens to get your token.
  Set it by running "SLACK_CLI_TOKEN='<token>' pomocli"`);
  signale.warn('Continuing without slack integration.');
}
const slack = program.slack && token;

async function execute() {
  const startTime = new Date();
  const endTime = addMinutes(startTime, mins);

  if (slack) {
    try {
      const status = await getStatus(token);
      const { profile: { status_text, status_emoji }} = status;
      origStatus.text = status_text;
      origStatus.emoji = status_emoji.replace(/:/gu, '');
      program.debug && signale.debug('origStatus', origStatus);

      setStatus(`${program.text} - ${mins} minutes remaining (${endTime.toLocaleTimeString('en-US')})`, program.emoji);
      startDnd(token, mins);
    } catch (error) {
      signale.error('Slack error. Continuing without slack integration.', error);
    }
  }

  signale.start(`Pomodoro will end at ${endTime.toLocaleTimeString('en-US')}`);
  progress.await(`${formatDistanceStrict(new Date(), endTime, { partialMethod: 'round' })} remaining`);

  intervalId = setInterval(() => {
    const msg = `${formatDistanceStrict(new Date(), endTime, { partialMethod: 'round' })} remaining`;
    slack && setStatus(`${program.text} - ${msg}`, program.emoji);
    progress.await(msg);
  }, 60000);

  setTimeout(endPomodoro, mins * 60 * 1000);
}

execute();

on_death(() => {
  console.log();
  program.debug && signale.debug('on_death');
  endPomodoro(true);

  setTimeout(() => {
    program.debug && signale.debug('exiting...');
    process.exit();
  }, 2000);
});

async function setStatus(text, emoji) {
  program.debug && signale.debug('setStatus', text, emoji);
  if (slack) {
    try {
      setSlackStatus(token, text, emoji);
    } catch (error) {
      signale.error('Failed to update slack status.', error);
    }
  }
}

async function endPomodoro(aborted = false) {
  program.debug && signale.debug('endPomodoro');
  clearInterval(intervalId);

  if (slack) {
    try {
      program.debug && signale.debug('endDnd');
      await endDnd(token);
      await setStatus(origStatus.text, origStatus.emoji);
      program.debug && signale.debug('after restore status');
    } catch (error) {
      signale.error('Failed to end do not disturb in Slack.', error);
    }
  }

  if (aborted) {
    program.notify && notifier.notify({
      title: 'Pomodoro',
      message: 'Aborted',
    });

    progress.fatal('Pomodoro aborted');
  } else {
    program.notify && notifier.notify({
      title: 'Pomodoro',
      message: 'Complete',
    });

    progress.success('Pomodoro complete');
  }
}
